export function createEmptyAddressObject() {
  return {
    name: '',
    street: '',
    country: '',
    postal_code: '',
    city: ''
  }
}

export function createEmptyPackageObject() {
  return {
    quantity: 1,
    weight: 0,
    dangerous_goods: false
  }
}

export function createEmptyCargoDetailsObject() {
  return {
    shipper_info: createEmptyAddressObject(),
    packages: [
      createEmptyPackageObject()
    ]
  }
}

export function createEmptyBookingObject() {
  return {
    client_reference: '',
    cargo_details: [
      createEmptyCargoDetailsObject()
    ],
    consignee_info: createEmptyAddressObject()
  }
}